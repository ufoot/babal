Babal SVG
=========

![Babal SVG screenshot](https://gitlab.com/ufoot/babal/raw/master/svg/screenshot-svg.jpg)

Babal is a bouncing ball game.

This is the source code for the
[SVG version](https://ufoot.org/software/babal/svg) of
[Babal](https://ufoot.org/software/babal).

You can try the [online demo](https://ufoot.org/demo/svgbabal/svgbabal.svg).

Status
------

This is really abandonware, I'm really not maintaining this any more.
Source code dates back from the early 2000s I thing.
Just sharing because I have no reasons not to share it.

Authors
-------

* Christian Mauduit <ufoot@ufoot.org> : main developper, project
  maintainer.

License
-------

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to [unlicense.org](https://unlicense.org)
