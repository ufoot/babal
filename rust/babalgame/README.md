Babalgame
=========

[Babal](https://ufoot.org/babal) is an arcade endless runner, with a bouncing ball.

[![Babal banner](https://gitlab.com/ufoot/babal/raw/master/banner.jpg)](https://ufoot.org/babal)

This is the game library, wraps up everything, linkable as a gdnative component.

Status
------

[![Build Status](https://gitlab.com/ufoot/babal/badges/master/pipeline.svg)](https://gitlab.com/ufoot/babal/pipelines)
[![Crates.io](https://img.shields.io/crates/v/babalgame.svg)](https://crates.io/crates/babalgame)
[![Docs](https://docs.rs/babalgame/badge.svg)](https://docs.rs/babalgame)

Links
-----

* [homepage](https://ufoot.org/babal) on ufoot.org
* [crate](https://crates.io/crates/babalgame) on crates.io
* [doc](https://docs.rs/babalgame/) on docs.rs
* [source](https://gitlab.com/ufoot/babal/tree/master/rust/babalgame) on gitlab.com

License
-------

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to [unlicense.org](https://unlicense.org)
