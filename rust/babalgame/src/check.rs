use gdnative::prelude::*;

#[derive(NativeClass)]
#[inherit(Node)]
pub struct Check;

pub const VERSION: &'static str = env!("CARGO_PKG_VERSION");

#[methods]
impl Check {
    fn new(_owner: &Node) -> Self {
        Check
    }

    #[export]
    fn ping(&self, _owner: &Node) -> GodotString {
        return GodotString::from_str("pong");
    }

    #[export]
    fn version(&self, _owner: &Node) -> GodotString {
        return GodotString::from_str(VERSION);
    }
}
