use gdnative::prelude::*;

mod ball;
mod check;
mod copyright;
mod input;
mod input_wrapper;
mod math_fn;
mod scene_helper;
mod tunnel;
mod tunnel_wrapper;
pub use ball::*;
pub use check::*;
pub use copyright::*;
pub use input::*;
pub use input_wrapper::*;
pub use math_fn::*;
pub use scene_helper::*;
pub use tunnel::*;
pub use tunnel_wrapper::*;

fn init(handle: InitHandle) {
    handle.add_class::<ball::Ball>();
    handle.add_class::<check::Check>();
    handle.add_class::<copyright::Copyright>();
    handle.add_class::<input::Input>();
    handle.add_class::<tunnel::Tunnel>();
    handle.add_class::<math_fn::MathFn>();
}

godot_init!(init);
