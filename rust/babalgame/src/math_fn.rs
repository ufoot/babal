use gdnative::prelude::*;

#[derive(NativeClass)]
#[inherit(Node)]
pub struct MathFn {}

#[methods]
impl MathFn {
    fn new(_owner: &Node) -> Self {
        MathFn {}
    }

    #[export]
    fn fade_in(&self, _owner: &Node, x: f64) -> f64 {
        babalcore::fade_in(x)
    }

    #[export]
    fn fade_out(&self, _owner: &Node, x: f64) -> f64 {
        babalcore::fade_out(x)
    }

    #[export]
    fn square_wave(&self, _owner: &Node, x: f64) -> f64 {
        babalcore::square_wave(x)
    }

    #[export]
    fn triangle_wave(&self, _owner: &Node, x: f64) -> f64 {
        babalcore::triangle_wave(x)
    }

    #[export]
    fn sawtooth_wave(&self, _owner: &Node, x: f64) -> f64 {
        babalcore::sawtooth_wave(x)
    }

    #[export]
    fn sine_wave(&self, _owner: &Node, x: f64) -> f64 {
        babalcore::sine_wave(x)
    }
}
