use gdnative::prelude::*;

#[derive(NativeClass)]
#[inherit(Label)]
pub struct Copyright;

#[methods]
impl Copyright {
    fn new(_owner: &Label) -> Self {
        Copyright
    }

    #[export]
    fn _ready(&self, owner: &Label) {
        owner.set_text("Babal (c) 2020-2021 Christian Mauduit");
    }
}
