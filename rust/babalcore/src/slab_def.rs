/// Different defs of slabs (those rectangles the ball can roll on).
/// A def is what defines a slab, it will be translated into a kind
/// at runtime. Typically some defs can be "a random slab", or
/// even "a slab that varies with time", etc.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum SlabDef {
    /// Nothing here, fall into the void.
    Void,
    /// Default slab def, a basic stuff one can roll on.
    Floor,
    /// When stumbing on this, players jumps at max speed.
    Boost,
}

impl SlabDef {
    pub fn is_special(&self) -> bool {
        match self {
            SlabDef::Void => false,
            SlabDef::Floor => false,
            SlabDef::Boost => true,
        }
    }
}

impl std::fmt::Display for SlabDef {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let c = match self {
            SlabDef::Void => " ",
            SlabDef::Floor => "#",
            SlabDef::Boost => "^",
        };
        write!(f, "{}", c)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fmt() {
        assert_eq!(" ", format!("{}", SlabDef::Void));
        assert_eq!("#", format!("{}", SlabDef::Floor));
        assert_eq!("^", format!("{}", SlabDef::Boost));
    }
}
