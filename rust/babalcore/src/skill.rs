use std::collections::HashMap;

use super::slab_def::*;

const SKILL_NOOB: &str = "noob";
const SKILL_EASY: &str = "easy";
const SKILL_MEDIUM: &str = "medium";
const SKILL_HARD: &str = "hard";
const SKILL_EXPERT: &str = "expert";

/// Different skills.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Skill {
    Noob,
    Easy,
    Medium,
    Hard,
    Expert,
}

impl Skill {
    pub fn from_str(name: &str) -> Self {
        match name {
            SKILL_NOOB => Skill::Noob,
            SKILL_EASY => Skill::Easy,
            SKILL_MEDIUM => Skill::Medium,
            SKILL_HARD => Skill::Hard,
            SKILL_EXPERT => Skill::Expert,
            &_ => Skill::default(),
        }
    }

    pub fn as_str(&self) -> &'static str {
        match self {
            Skill::Noob => SKILL_NOOB,
            Skill::Easy => SKILL_EASY,
            Skill::Medium => SKILL_MEDIUM,
            Skill::Hard => SKILL_HARD,
            Skill::Expert => SKILL_EXPERT,
        }
    }

    pub fn width(&self) -> usize {
        match self {
            Skill::Noob => 8,
            Skill::Easy => 9,
            Skill::Medium => 10,
            Skill::Hard => 11,
            Skill::Expert => 12,
        }
    }

    pub fn def_density(self) -> HashMap<SlabDef, f64> {
        let mut ret = HashMap::new();
        match self {
            Skill::Noob => {
                ret.insert(SlabDef::Floor, 0.93);
                ret.insert(SlabDef::Boost, 0.02);
            }
            Skill::Easy => {
                ret.insert(SlabDef::Floor, 0.83);
                ret.insert(SlabDef::Boost, 0.02);
            }
            Skill::Medium => {
                ret.insert(SlabDef::Floor, 0.7);
                ret.insert(SlabDef::Boost, 0.05);
            }
            Skill::Hard => {
                ret.insert(SlabDef::Floor, 0.6);
                ret.insert(SlabDef::Boost, 0.1);
            }
            Skill::Expert => {
                ret.insert(SlabDef::Floor, 0.5);
                ret.insert(SlabDef::Boost, 0.1);
            }
        }
        ret
    }

    pub fn def_density0(self) -> HashMap<SlabDef, f64> {
        let density = self.def_density();
        let mut total: f64 = 0.0;
        for (_, v) in density {
            total += v;
        }
        let mut ret = HashMap::new();
        ret.insert(SlabDef::Floor, total);
        ret
    }

    pub fn mutability(&self) -> f64 {
        match self {
            Skill::Noob => 0.05,
            Skill::Easy => 0.1,
            Skill::Medium => 0.2,
            Skill::Hard => 0.3,
            Skill::Expert => 0.5,
        }
    }

    pub fn time_to_complete(self) -> f64 {
        match self {
            Skill::Noob => 30.0,
            Skill::Easy => 90.0,
            Skill::Medium => 120.0,
            Skill::Hard => 180.0,
            Skill::Expert => 300.0,
        }
    }

    pub fn begin_speed_factor(self) -> f64 {
        match self {
            Skill::Noob => 0.7,
            Skill::Easy => 0.9,
            Skill::Medium => 1.0,
            Skill::Hard => 1.2,
            Skill::Expert => 1.3,
        }
    }

    pub fn end_speed_factor(self) -> f64 {
        match self {
            Skill::Noob => 0.7,
            Skill::Easy => 1.0,
            Skill::Medium => 1.2,
            Skill::Hard => 1.4,
            Skill::Expert => 1.7,
        }
    }
}

impl std::default::Default for Skill {
    fn default() -> Self {
        Skill::Medium
    }
}
