/// A trait to abstract the input query interface, it is mostly
/// used for testing the game logic without having real inputs.
pub trait InputQuery {
    /// Returns true if a jump is requested. It can return
    /// true several times in a row, even if the button/key
    /// has been pressed once, but this only for a limited time.
    /// OTOH if you call pop_jump very late after the button/key
    /// press, it will still buffer it and return true.
    fn pop_jump(&mut self) -> bool;
    /// Returns steering information, that is, how much on the
    /// left or on the right the player should move. The values
    /// accumulate over time so popping values after 50 msec
    /// is likely to return twice the value popped after 25 msec.
    fn pop_steer(&mut self) -> f64;
}
