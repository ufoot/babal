/// A maximum width, most playable values should range from 6 to 12,
/// so 32 is already a lot. To simplify/optimize code, program uses
/// fixed size rows, set to the WIDTH_MAX. With slabs of 4 or 8 bytes,
/// it means only 256 bytes (worst case) for a row. With this setting,
/// a 10_000 rows level, playable for about an hour, at 3 slabs/sec still
/// fits into 30Mb of memory.
pub const WIDTH_MAX: usize = 32;

/// Indices for rows can not be bigger than this. It is insanely big though,
/// at 100 slabs per sec, it represents 1 million seconds, which is more than
/// 10 days of continuous play.
pub const ABSOLUTE_LENGTH_MAX: usize = 100_000_000;

/// History about one level will be kept only below this limit.
/// Above it, the initial slabs will disappear. This should really
/// not be a problem as for a complete restart, we can reinitialize with
/// a seed, and otherwise it stills allows going back in time by 10k slabs,
/// which means several minutes even at high speed.
pub const SLIDING_LENGTH_MAX: usize = 10_000;
