use super::slab_def::*;
use super::slab_kind::*;

#[derive(Debug, Clone, Copy)]
pub struct Slab {
    def: SlabDef,
}

impl Slab {
    pub fn new(def: SlabDef) -> Slab {
        Slab { def }
    }

    pub fn slab_kind(&self, _now_msec: i64) -> SlabKind {
        match &self.def {
            SlabDef::Void => SlabKind::Void,
            SlabDef::Floor => SlabKind::Floor,
            SlabDef::Boost => SlabKind::Boost,
        }
    }

    pub fn get_item(&self, now_msec: i64) -> i64 {
        self.slab_kind(now_msec).as_item()
    }

    pub fn set(&mut self, def: SlabDef) {
        self.def = def
    }

    pub fn get(&self) -> SlabDef {
        self.def
    }
}

impl std::fmt::Display for Slab {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.slab_kind(0))
    }
}

impl std::default::Default for Slab {
    fn default() -> Self {
        Self::new(SlabDef::Void)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fmt() {
        assert_eq!(" ", format!("{}", Slab::new(SlabDef::Void)));
        assert_eq!("#", format!("{}", Slab::new(SlabDef::Floor)));
        assert_eq!("^", format!("{}", Slab::new(SlabDef::Boost)));
    }
}
