use std::time::Instant;

/// Return the time contained in the option if it is defined.
/// If not (None was passed) returns the result of an actual
/// call to Instant::now(). This is useful for testing: in
/// mainstream production code, just pass None, but for
/// testing it is possible to pass fake instants.
///
/// ```
/// use std::time::Instant;
/// use babalcore::*;
///
/// // Use default.
/// let t1 = unwrap_now(None);
/// // Override with custom instant.
/// let t2 = unwrap_now(Some(Instant::now()));
///
/// assert!(t1 <= t2);
/// ```
pub fn unwrap_now(now: Option<Instant>) -> Instant {
    match now {
        Some(n) => n,
        None => Instant::now(),
    }
}

#[cfg(test)]
mod tests {
    use crate::*;
    use std::time::{Duration, Instant};

    #[test]
    fn test_default() {
        let now = unwrap_now(None);
        assert!(now <= Instant::now());
        let before = unwrap_now(Some(now - Duration::from_secs(1)));
        assert!(before < now);
    }
}
