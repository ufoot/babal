/// Return a string describing the type.
///
/// ```
/// use babalcore::*;
///
/// #[derive(Debug)]
/// struct MyType {}
///
/// let i = 0i64;
/// let v = MyType{};
/// assert_eq!(format!("i64"), format_type_of(&i));
/// assert_eq!(format!("rust_out::main::MyType"), format_type_of(&v));
/// ```
pub fn format_type_of<T>(_: &T) -> String {
    format!("{}", std::any::type_name::<T>())
}
