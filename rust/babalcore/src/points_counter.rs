use std::collections::HashMap;

use super::player_event::*;
use super::time_tracker::*;

#[derive(Debug, Default)]
pub struct PointsCounter {
    total_points: f64,
    last_dist: f64,
    last_row: isize,
    last_best_dist: f64,
    last_best_row: isize,
    time_played_tracker: TimeTracker,
    delta_total: f64,
    scores_log: HashMap<isize, f64>,
    behind: bool,
    best_current_score: i32,
    best_previous_score: i32,
    high_score: bool,
    best_time_pct: i32,
}

const INSTANT_SPEED_WEIGHT: f64 = 3.0;
const AVERAGE_SPEED_WEIGHT: f64 = 1.0;

impl PointsCounter {
    pub fn new() -> PointsCounter {
        PointsCounter::default()
    }

    pub fn handle_dist(
        &mut self,
        delta: f64,
        dist: f64,
        time_to_complete: f64,
    ) -> Vec<PlayerEvent> {
        if delta <= 0.0 {
            return Vec::new();
        }
        let delta_best_dist = dist - self.last_best_dist;
        if delta_best_dist > 0.0 && !self.behind {
            self.time_played_tracker.add(delta);
        }
        self.delta_total += delta;
        if self.delta_total <= 0.0 {
            return Vec::new();
        }
        let mut ret = Vec::new();
        if delta_best_dist > 0.0 {
            let average_speed = dist / self.delta_total;
            self.total_points += AVERAGE_SPEED_WEIGHT * delta * average_speed;
            let instant_speed = delta_best_dist / delta;
            self.total_points += INSTANT_SPEED_WEIGHT * delta_best_dist * instant_speed;
            self.last_best_dist = dist;
            self.score_to_log(dist);
        } else {
            self.score_from_log(dist);
        }

        let row = dist.round() as isize;
        if row >= self.last_best_row {
            // Checking the "catch-up" event with integer values as in practice
            // the ball might move a little forward when falling.
            if self.behind {
                ret.push(PlayerEvent::new_catch_up());
                self.behind = false;
            }
            self.last_best_row = row;
        } else {
            self.behind = true;
        }

        self.last_dist = dist;
        self.last_row = row;

        let score = self.total_points.floor() as i32;
        if self.best_current_score < score {
            if score / 1000 > self.best_current_score / 1000 {
                ret.push(PlayerEvent::new_thousand_score((score / 1000) * 1000));
            }
            self.best_current_score = score;
        }
        if self.best_previous_score > 0 && self.best_previous_score < score && !self.high_score {
            self.high_score = true;
            ret.push(PlayerEvent::new_high_score(score));
        }
        let time_pct = self.time_pct(time_to_complete).floor() as i32;
        if self.best_time_pct < time_pct {
            if time_pct >= 100 {
                ret.push(PlayerEvent::new_finish(score));
            } else {
                let step = if time_to_complete < 50.0 {
                    20
                } else if time_to_complete > 200.0 {
                    5
                } else {
                    10
                };
                if time_pct / step > self.best_time_pct / step
                    || time_pct == 50
                    || time_pct == 90
                    || time_pct == 95
                    || time_pct == 97
                    || time_pct == 98
                    || time_pct == 99
                {
                    ret.push(PlayerEvent::new_percent_time(time_pct));
                }
            }
            self.best_time_pct = time_pct;
        }

        ret
    }

    fn score_to_log(&mut self, dist: f64) {
        self.scores_log
            .insert(dist.round() as isize, self.total_points);
    }

    fn score_from_log(&mut self, dist: f64) {
        self.total_points = *self
            .scores_log
            .get(&(dist.round() as isize))
            .unwrap_or(&0.0);
    }

    pub fn score(&self) -> i32 {
        let tmp: f64 = self.total_points;
        tmp as i32
    }

    pub fn set_best_score(&mut self, best_score: i32) {
        self.best_previous_score = best_score;
    }

    pub fn best_row(&self) -> isize {
        self.last_best_dist.round() as isize
    }

    pub fn best_dist(&self) -> f64 {
        self.last_best_dist
    }

    pub fn time_pct(&self, time_to_complete: f64) -> f64 {
        if time_to_complete <= 0.0 {
            0.0
        } else {
            let pct = 100.0 * self.time_played_tracker.now_sec() / time_to_complete;
            if pct < 100.0 {
                pct
            } else {
                100.0
            }
        }
    }
}
