use super::skill::*;

pub trait LevelQuery {
    fn width(&self) -> usize;
    fn len(&self) -> usize;
    fn first(&self) -> isize;
    fn last(&self) -> isize;
    fn item(&self, col: isize, row: isize, boost: bool) -> i64;
    fn find_start_spot(&self, col: isize, row: isize) -> Option<(isize, isize)>;
    fn skill(&self) -> Skill;
}
