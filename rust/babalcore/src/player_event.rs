use super::player_event_kind::*;

pub struct PlayerEvent {
    pub kind: PlayerEventKind,
    pub arg: Option<i32>,
}

impl PlayerEvent {
    pub fn new_start() -> Self {
        PlayerEvent {
            kind: PlayerEventKind::Start,
            arg: None,
        }
    }
    pub fn new_jump() -> Self {
        PlayerEvent {
            kind: PlayerEventKind::Jump,
            arg: None,
        }
    }
    pub fn new_land() -> Self {
        PlayerEvent {
            kind: PlayerEventKind::Land,
            arg: None,
        }
    }
    pub fn new_fall() -> Self {
        PlayerEvent {
            kind: PlayerEventKind::Fall,
            arg: None,
        }
    }
    pub fn new_catch_up() -> Self {
        PlayerEvent {
            kind: PlayerEventKind::CatchUp,
            arg: None,
        }
    }
    pub fn new_change_column() -> Self {
        PlayerEvent {
            kind: PlayerEventKind::ChangeColumn,
            arg: None,
        }
    }
    pub fn new_boost() -> Self {
        PlayerEvent {
            kind: PlayerEventKind::Boost,
            arg: None,
        }
    }
    pub fn new_overdrive() -> Self {
        PlayerEvent {
            kind: PlayerEventKind::Overdrive,
            arg: None,
        }
    }
    pub fn new_thousand_score(score: i32) -> Self {
        PlayerEvent {
            kind: PlayerEventKind::ThousandScore,
            arg: Some(score),
        }
    }
    pub fn new_percent_time(percent: i32) -> Self {
        PlayerEvent {
            kind: PlayerEventKind::PercentTime,
            arg: Some(percent),
        }
    }
    pub fn new_high_score(score: i32) -> Self {
        PlayerEvent {
            kind: PlayerEventKind::HighScore,
            arg: Some(score),
        }
    }
    pub fn new_finish(score: i32) -> Self {
        PlayerEvent {
            kind: PlayerEventKind::Finish,
            arg: Some(score),
        }
    }
}

impl std::fmt::Display for PlayerEvent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.arg {
            Some(v) => write!(f, "{}: {}", self.kind.as_str(), v),
            None => write!(f, "{}", self.kind.as_str()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fmt() {
        assert_eq!("start", format!("{}", PlayerEvent::new_start()));
        assert_eq!("jump", format!("{}", PlayerEvent::new_jump()));
        assert_eq!("land", format!("{}", PlayerEvent::new_land()));
        assert_eq!("fall", format!("{}", PlayerEvent::new_fall()));
        assert_eq!("catch_up", format!("{}", PlayerEvent::new_catch_up()));
        assert_eq!(
            "change_column",
            format!("{}", PlayerEvent::new_change_column())
        );
        assert_eq!("boost", format!("{}", PlayerEvent::new_boost()));
        assert_eq!("overdrive", format!("{}", PlayerEvent::new_overdrive()));
        assert_eq!(
            "thousand_score: 42",
            format!("{}", PlayerEvent::new_thousand_score(42))
        );
        assert_eq!(
            "percent_time: 42",
            format!("{}", PlayerEvent::new_percent_time(42))
        );
        assert_eq!(
            "high_score: 42",
            format!("{}", PlayerEvent::new_high_score(42))
        );
        assert_eq!("finish: 42", format!("{}", PlayerEvent::new_finish(42)));
    }
}
