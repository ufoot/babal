Babal
=====

[Babal](https://ufoot.org/babal) is an arcade endless runner, with a bouncing ball.

[![Babal banner](https://gitlab.com/ufoot/babal/raw/master/banner.jpg)](https://ufoot.org//babal)


Status
------

[![Build Status](https://gitlab.com/ufoot/babal/badges/master/pipeline.svg)](https://gitlab.com/ufoot/babal/pipelines)

Rules
-----

You control a ball in a rolling tunnel, try to stay on the track,
and not fall into space void. Controls are very simple, turn left, turn
right, jump, that's all. Stepping on blue slabs will boost your ball and make you jump and speed up,
increasing your score. The faster you go go, the higher the score.

Credits
-------

Babal uses [Godot engine](https://godotengine.org) and [godot-rust](https://godot-rust.github.io/).

Music has been written by Alexander Nakarada from [Serpent Sound Studios](https://www.serpentsoundstudios.com).

Get the game
------------

* On [Google Play Store](https://play.google.com/store/apps/details?id=org.ufoot.babal) for Android devices (also on [Amazon Appstore](https://www.amazon.com/dp/B08VFF3V2J))
* On [itch.io](https://ufoot.itch.io/babal) for desktop platforms (Windows, Mac OS X, GNU/Linux)
* [Direct downloads](https://ufoot.org/download/babal/godot) to get access to all archives and previous versions

Authors
-------

* [Christian Mauduit](mailto:ufoot@ufoot.org) : main developper, project
  maintainer.

License
-------

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to [unlicense.org](https://unlicense.org)
