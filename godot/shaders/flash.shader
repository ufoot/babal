shader_type canvas_item;

uniform float blend = 0.9;
uniform float radius_weight = 1.0;
uniform float center_weight = 1.0;
uniform float color_1_red = 0.0;
uniform float color_1_green = 1.0;
uniform float color_1_blue = 1.0;
uniform float color_2_red = 1.0;
uniform float color_2_green = 1.0;
uniform float color_2_blue = 1.0;

void fragment(){
	vec4 c1_outside = vec4(color_1_red, color_1_green, color_1_blue, 1.0);
	vec4 c2_outside = vec4(color_2_red, color_2_green, color_2_blue, 1.0);
	vec4 c1_center = c1_outside;
	vec4 c2_center = c2_outside;
	c1_center[3] = 0.0;
	c2_center[3] = 0.0;
	float b = blend;
	if (b>1.0) {
		b=1.0;
	}
	if (b<0.0) {
		b=0.0;
	}
	vec4 c_outside = mix(c1_outside, c2_outside, b);
	vec4 c_center = mix(c1_center, c2_center, b);
	vec2 to_center = UV - vec2(0.5, 0.5);
	float r = length(to_center) * 2.0;
	if (r > 1.0) {
		r = 1.0;
	}
	b =1.0-b;
    b = b*(r*radius_weight+center_weight)/(radius_weight+center_weight); 
	vec4 c_final = mix(c_center, c_outside, b);
	COLOR.rgba = c_final;
}
