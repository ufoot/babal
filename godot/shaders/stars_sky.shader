shader_type canvas_item;

float noise(vec2 v) {
	return fract(101.0*sin(v.x*103.0+v.y*7.0)*cos(v.x*107.0+v.y*5.0)*(1.0+fract(log(1.0+length(v)))));
}

void fragment(){
	float noise = noise(UV);
	float f = fract(TIME/3600.0+noise);
	if (f <= 0.001) {
		if (f <= 0.00025) {
			f *= 4000.0;
		} else if (f >= 0.00075){
			f = (0.001-f) * 4000.0;
		} else {
			f = 1.0;
		}
	} else {
		f = 0.0;
	}
	COLOR = vec4(f, f, f, 1.0);
}