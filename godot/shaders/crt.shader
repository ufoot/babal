shader_type canvas_item;

uniform float blend = 0.5;
uniform float retro_screen_x = 400;
uniform float retro_screen_y = 225;

void fragment(){
	float scale_x = round(max(1.0,1.0/(retro_screen_x*SCREEN_PIXEL_SIZE.x)));
	float scale_y = round(max(1.0,1.0/(retro_screen_y*SCREEN_PIXEL_SIZE.y)));
	vec3 c1 = textureLod(SCREEN_TEXTURE, SCREEN_UV, 0.0).rgb;
	vec3 c2 = c1;
	int j = int(SCREEN_UV.y/(SCREEN_PIXEL_SIZE.y*scale_y));
	float y_pick = (float(j-j%3)+1.5)*scale_y*SCREEN_PIXEL_SIZE.y;
	if (j%3 ==2) {
	    c2 = vec3(0.0, 0.0, 0.0);
	} else {
		vec2 dx = vec2(SCREEN_PIXEL_SIZE.x, 0.0);
		vec2 dy = vec2(SCREEN_PIXEL_SIZE.y, 0.0);
	
		int i = int(SCREEN_UV.x/(SCREEN_PIXEL_SIZE.x*scale_x));
		float x_pick = ((float(i-i%4))+2.0)*scale_x*SCREEN_PIXEL_SIZE.x;
		float lod = log2(scale_x + scale_y) / 2.0;
		int i_mod_4 = int(i%4);
		if (i_mod_4 == 0) {
			c2.rgb.r=textureLod(SCREEN_TEXTURE, vec2(x_pick, y_pick), lod).rgb.r;
			c2.rgb.b=0.0;
			c2.rgb.g=0.0;
		} else if (i_mod_4 == 1) {
			c2.rgb.r=0.0;
			c2.rgb.g=textureLod(SCREEN_TEXTURE, vec2(x_pick, y_pick), lod).rgb.g;
			c2.rgb.b=0.0;
		} else if (i_mod_4 == 2) {
			c2.rgb.r=0.0;
			c2.rgb.g=0.0;
			c2.rgb.b=textureLod(SCREEN_TEXTURE, vec2(x_pick, y_pick), lod).rgb.b;
		} else if (i_mod_4 == 3) {
			c2.rgb.r=0.0;
			c2.rgb.g=0.0;
			c2.rgb.b=0.0;
		}
	}
	float b = 1.0-blend;
	if (b>1.0) {
		b=1.0;
	}
	if (b<0.0) {
		b=0.0;
	}
    COLOR.rgb = mix(c1,c2,b);
}
