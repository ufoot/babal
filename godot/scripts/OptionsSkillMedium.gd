extends CheckBox

var global

func _ready():
	global = get_node("/root/Global")
	if global.skill == "medium":
		self.pressed = true
		
func _pressed():
	global.skill = "medium"
	global.save_config()
