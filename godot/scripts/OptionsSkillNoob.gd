extends CheckBox

var global

func _ready():
	global = get_node("/root/Global")
	if global.skill == "noob":
		self.pressed = true
		
func _pressed():
	global.skill = "noob"
	global.save_config()
