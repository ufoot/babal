extends Label

var error_msg = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	self.visible = false
	var global = get_node("/root/Global")
	var check_class = preload("res://scripts/native/Check.gdns")
	if not check_class:
		display_error("Lib not found")
		return
	var check = check_class.new()
	if not check:
		display_error("No instance")
		return
	var pong = check.ping()
	if pong != "pong":
		display_error("Bad pong "+pong)
	var version = check.version()
	if version != global.VERSION:
		display_error("Bad version "+ version)

func display_error(msg):
	error_msg = msg
	self.visible = true
	self.get_parent().get_child(0).visible=false
	self.text += "\n\nError: " + msg

func _input(event):
	if error_msg == "":
		return
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			get_tree().quit()
	#if event is InputEventScreenTouch:
	#	if event.index == 0 and !event.pressed:
	#		get_tree().quit()

