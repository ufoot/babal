extends TextureButton

var global

func _ready():
	global = get_node("/root/Global")
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()

func _pressed():
	global.reset_config()
	if get_tree().change_scene("res://scenes/OptionsMenu.tscn"):
		pass

func setup_layout():
	var viewport_size = get_viewport_rect().size
	var wh = (viewport_size.x+viewport_size.y)/7
	rect_scale.x = wh/rect_size.x
	rect_scale.y = wh/rect_size.y
	rect_position.x = viewport_size.x - wh
	rect_position.y = viewport_size.y - wh
