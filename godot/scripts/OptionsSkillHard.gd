extends CheckBox

var global

func _ready():
	global = get_node("/root/Global")
	if global.skill == "hard":
		self.pressed = true
		
func _pressed():
	global.skill = "hard"
	global.save_config()
