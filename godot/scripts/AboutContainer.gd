extends VBoxContainer

func _ready():
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()

func setup_layout():
	self.rect_position.y = 0
	self.rect_min_size.x = get_viewport_rect().size.x*0.7
	self.rect_size.x = self.rect_min_size.x
	self.rect_position.x = (get_viewport_rect().size.x-self.rect_size.x)/2
	for child in get_children():
		child.rect_min_size.x = self.rect_min_size.x
		child.rect_size.x = self.rect_size.x
