extends CheckButton

var global

func _ready():
	global = get_node("/root/Global")
	pressed = global.show_fps

func _toggled(button_pressed):
	global.show_fps = bool(button_pressed)
	global.save_config()
