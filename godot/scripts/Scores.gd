var scores

const SCORES_CFG_PATH = "user://scores.cfg"
const NB_SCORES = 5

class ScoreSorter:
	static func sort_descending(a, b):
		return a["points"] > b["points"]

func _init():
	scores=ConfigFile.new()
	if scores.load(SCORES_CFG_PATH) != OK:
		print("unable to load scores from %s" % SCORES_CFG_PATH)
		return
	print("load scores from %s" % SCORES_CFG_PATH)

func get_section_name(i):
	return "score_%d" % i

func get_score(i):
	var score={}
	var section_name = get_section_name(i)
	score["points"]=int(scores.get_value(section_name, "points", 0))
	score["skill"]=str(scores.get_value(section_name, "skill", "medium"))
	score["year"]=int(scores.get_value(section_name, "year", 1970))
	score["month"]=int(scores.get_value(section_name, "month", 1))
	score["day"]=int(scores.get_value(section_name, "day", 1))
	score["hour"]=int(scores.get_value(section_name, "hour", 0))
	score["minute"]=int(scores.get_value(section_name, "minute", 0))
	score["second"]=int(scores.get_value(section_name, "second", 0))
	return score

func get_best_score():
	return get_score(0)

func get_best_score_points():
	var score = get_best_score()
	if not score.has("points"):
		return 0
	return int(score["points"])

func add_score(score):
	if (not score.has("points")) or (not score.has("skill")):
		print("incorrect score %s", str(score))
		return
	var new_score={}
	new_score["points"]=int(score["points"])
	new_score["skill"]=str(score["skill"])
	var now = OS.get_datetime()
	new_score["year"] = int(now["year"])
	new_score["month"] = int(now["month"])
	new_score["day"] = int(now["day"])
	new_score["hour"] = int(now["hour"])
	new_score["minute"] = int(now["minute"])
	new_score["second"] = int(now["second"])
	var all_scores=[new_score]
	for i in range(0, NB_SCORES):
		all_scores.append(get_score(i))
	all_scores.sort_custom(ScoreSorter, "sort_descending")
	for i in range(0, NB_SCORES):
		var section_name = get_section_name(i)
		for j in all_scores[i].keys():
			scores.set_value(section_name, j, all_scores[i][j])

func save_scores():
	if scores.save(SCORES_CFG_PATH) != OK:
		print("unable to save scores to %s" % SCORES_CFG_PATH)
	print("save scores to %s" % SCORES_CFG_PATH)

func pretty_print(i):
	var score = get_score(i)
	return "%04d-%02d-%02d %02d:%02d - %s - %d" % [
		score["year"], score["month"], score["day"],
		score["hour"], score["minute"],
		score["skill"], score["points"]]

