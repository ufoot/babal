extends Control

var global

func _ready():
	global = get_node("/root/Global")
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()

func setup_layout():
	var size = get_viewport_rect().size
	var wh = int((size.x + size.y)/14)
	$EscButton.rect_position.x = 0
	$EscButton.rect_position.y = 0
	$EscButton.rect_scale.x = wh/$EscButton.rect_size.x
	$EscButton.rect_scale.y = wh/$EscButton.rect_size.y
