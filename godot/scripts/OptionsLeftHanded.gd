extends CheckButton

var global

func _ready():
	global = get_node("/root/Global")
	pressed = global.left_handed

func _toggled(button_pressed):
	global.left_handed = bool(button_pressed)
	global.save_config()
