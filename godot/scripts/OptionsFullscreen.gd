extends CheckBox

var global

func _ready():
	global = get_node("/root/Global")

func _process(_delta):
	pressed = OS.window_fullscreen
	disabled = global.mobile_ui()
	
func _pressed():
	global.fullscreen = true
	global.save_config()
	self.pressed = OS.window_fullscreen
	global.save_config()
	
