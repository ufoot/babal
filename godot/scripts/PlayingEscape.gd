extends Control

func _ready():
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()

func setup_layout():
	rect_position.x=0
	rect_position.y=0
	var viewport_size = get_viewport_rect().size
	var center = Vector2(viewport_size.x / 2, viewport_size.y /2)
	$Dim.rect_size = viewport_size
	var wh = (viewport_size.x+viewport_size.y)/7
	for button in get_children():
		if button is TextureButton:
			button.rect_scale.x = wh/button.rect_size.x
			button.rect_scale.y = wh/button.rect_size.y
	$Menu.rect_position.x = center.x - 1.5*wh
	$Menu.rect_position.y = center.y - 0.5*wh
	$Play.rect_position.x = center.x - 0.5*wh
	$Play.rect_position.y = center.y - 0.5*wh
	$Restart.rect_position.x = center.x + 0.5*wh
	$Restart.rect_position.y = center.y - 0.5*wh
