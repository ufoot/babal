extends Control

const DEMO_Z_SPEED = 3
const DEMO_X_SPEED = 0.5

var global
var player_position
var player_offset
var player_base

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	global = get_node("/root/Global")
	$World/Tunnel.setup("res://scenes/Rail.tscn", global.skill, 0)
	player_offset = Vector3(0,0,0)
	player_base=$World/Ball.position()

func _process(delta):
	player_offset.z += DEMO_Z_SPEED * delta
	player_offset.x += DEMO_X_SPEED * delta
	player_position=player_base+player_offset
	$World/Tunnel.set_level_row(int(player_position.z))
	player_position.z=player_position.z-int(player_position.z)
	$World/Tunnel.update_player_position(player_position)
	$World/Tunnel.sync()

func on_escape():
	get_tree().quit()

