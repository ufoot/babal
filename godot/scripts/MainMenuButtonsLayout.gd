extends Control

func _ready():
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()

func setup_layout():
	rect_position=Vector2(0,0)
	var viewport_size = get_viewport_rect().size
	var center = Vector2(viewport_size.x / 2, viewport_size.y /2)
	var wh = (viewport_size.x+viewport_size.y)/7
	for button in get_children():
		button.rect_scale.x = wh/button.rect_size.x
		button.rect_scale.y = wh/button.rect_size.y
	$Play.rect_position.x = center.x - 1.5*wh
	$Play.rect_position.y = center.y - wh
	$Scores.rect_position.x = center.x - 0.5*wh
	$Scores.rect_position.y = center.y - wh
	$Options.rect_position.x = center.x + 0.5*wh
	$Options.rect_position.y = center.y - wh
	$About.rect_position.x = center.x - 1.5*wh
	$About.rect_position.y = center.y
	$Homepage.rect_position.x = center.x - 0.5*wh
	$Homepage.rect_position.y = center.y
	$Rate.rect_position.x = center.x - 0.5*wh
	$Rate.rect_position.y = center.y
	$Quit.rect_position.x = center.x + 0.5*wh
	$Quit.rect_position.y = center.y
