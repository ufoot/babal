extends Node

const VERSION = "0.5.1"

const CONFIG_CFG_PATH = "user://config.cfg"

const DEFAULT_SKILL = "medium"
const DEFAULT_INVERT_X = false
const DEFAULT_LEFT_HANDED = false
const DEFAULT_INPUT_SENSIBILITY = 1.0
const DEFAULT_ROTATION_SPEED = 1.0
const DEFAULT_FULLSCREEN = true
const DEFAULT_MASTER_VOLUME = 90
const DEFAULT_MUSIC_VOLUME = 90
const DEFAULT_SFX_VOLUME = 90
const DEFAULT_FORCE_MOBILE_UI = false
const DEFAULT_USE_SHADERS = true
const DEFAULT_SHOW_FPS = false

var skill = DEFAULT_SKILL
var invert_x = DEFAULT_INVERT_X
var left_handed = DEFAULT_LEFT_HANDED
var input_sensibility = DEFAULT_INPUT_SENSIBILITY
var rotation_speed = DEFAULT_ROTATION_SPEED
var fullscreen = DEFAULT_FULLSCREEN
var master_volume = DEFAULT_MASTER_VOLUME
var music_volume = DEFAULT_MUSIC_VOLUME
var sfx_volume = DEFAULT_SFX_VOLUME
var force_mobile_ui = DEFAULT_FORCE_MOBILE_UI
var use_shaders = DEFAULT_USE_SHADERS
var show_fps = DEFAULT_SHOW_FPS

var config

func _ready():
	read_config()

func read_config():
	config=ConfigFile.new()
	if config.load(CONFIG_CFG_PATH) != OK:
		reset_config()
		return
	print("load config from "+ CONFIG_CFG_PATH)
	skill = str(config.get_value("game" , "skill", DEFAULT_SKILL))
	invert_x = bool(config.get_value("control" , "invert_x", DEFAULT_INVERT_X))
	left_handed = bool(config.get_value("control" , "left_handed", DEFAULT_LEFT_HANDED))
	input_sensibility = float(config.get_value("control" , "input_sensibility", DEFAULT_INPUT_SENSIBILITY))
	rotation_speed = float(config.get_value("control" , "rotation_speed", DEFAULT_ROTATION_SPEED))
	fullscreen = bool(config.get_value("video" , "fullscreen", DEFAULT_FULLSCREEN))
	master_volume = float(config.get_value("audio" , "master_volume", DEFAULT_MASTER_VOLUME))
	music_volume = float(config.get_value("audio" , "music_volume", DEFAULT_MUSIC_VOLUME))
	sfx_volume = float(config.get_value("audio" , "sfx_volume", DEFAULT_SFX_VOLUME))
	force_mobile_ui= bool(config.get_value("debug" , "force_mobile_ui", DEFAULT_FORCE_MOBILE_UI))
	use_shaders = bool(config.get_value("debug" , "use_shaders", DEFAULT_USE_SHADERS))
	show_fps = bool(config.get_value("debug" , "show_fps", DEFAULT_SHOW_FPS))
	apply_config()

func reset_config():
	skill = DEFAULT_SKILL
	invert_x = DEFAULT_INVERT_X
	left_handed = DEFAULT_INVERT_X
	input_sensibility = DEFAULT_INPUT_SENSIBILITY
	rotation_speed = DEFAULT_ROTATION_SPEED
	fullscreen = DEFAULT_FULLSCREEN
	master_volume = DEFAULT_MASTER_VOLUME
	music_volume = DEFAULT_MUSIC_VOLUME
	sfx_volume = DEFAULT_SFX_VOLUME
	force_mobile_ui = DEFAULT_FORCE_MOBILE_UI
	use_shaders = DEFAULT_USE_SHADERS
	show_fps = DEFAULT_SHOW_FPS
	if not config:
		return
	for section in config.get_sections():
		config.erase_section(section)
	save_config()

func save_config():
	if not config:
		return
	print("save config to "+ CONFIG_CFG_PATH)
	config.set_value("game", "skill", str(skill))
	config.set_value("control", "invert_x", bool(invert_x))
	config.set_value("control", "left_handed", bool(left_handed))
	config.set_value("control", "input_sensibility", bool(input_sensibility))
	config.set_value("control", "rotation_speed", bool(rotation_speed))
	config.set_value("video", "fullscreen", bool(fullscreen))
	config.set_value("audio", "master_volume", float(master_volume))
	config.set_value("audio", "music_volume", float(music_volume))
	config.set_value("audio", "sfx_volume", float(sfx_volume))
	config.set_value("debug", "force_mobile_ui", bool(force_mobile_ui))
	config.set_value("debug", "use_shaders", bool(use_shaders))
	config.set_value("debug", "show_fps", bool(show_fps))
	config.save(CONFIG_CFG_PATH)
	apply_config()

func mobile_ui():
	var show = force_mobile_ui or OS.has_touchscreen_ui_hint()
	return show

func is_android():
	var show = force_mobile_ui or OS.get_name() == "Android"
	return show

func apply_config():
	OS.window_fullscreen = fullscreen
	var index
	var db
	index = AudioServer.get_bus_index("Master")
	db = (sqrt(master_volume)-10)*10
	AudioServer.set_bus_volume_db(index, db)
	index = AudioServer.get_bus_index("Music")
	db = (sqrt(music_volume)-10)*10
	AudioServer.set_bus_volume_db(index, db)
	index = AudioServer.get_bus_index("SFX")
	db = (sqrt(sfx_volume)-10)*10
	AudioServer.set_bus_volume_db(index, db)
