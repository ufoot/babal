extends RichTextLabel

func _ready():
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()

func setup_layout():
	self.rect_min_size.y = get_viewport_rect().size.y/2
