extends Control

var escape_mode = false
var global

var time_progress_done = 0
var time_progress_total = 0
var current_score = 0
var finished = false
var finished_acc = 0.0
var final_score = false
var new_record = false
var boost_acc = 0.0
var is_boosting = false

var fall_acc = 0.0
var is_falling = false

var jump_acc = 1.0

var message_fx_acc = 0.0
var message_txt_acc = 0.0

var math_fn = preload("res://scripts/native/MathFn.gdns").new()
var scores_class = preload("res://scripts/Scores.gd")
var scores
var score_saved = false

var delta_since_last_land = 1.0
var delta_since_last_jump = 1.0
var delta_since_last_boost = 1.0

var player_position

const BOOST_DURATION = 1.5
const FALL_DURATION = 0.7
const MESSAGE_FX_DURATION = 0.5
const MESSAGE_TXT_DURATION = 1.5
const JUMP_OVERSCALE_DURATION = 0.5
const FINISH_DURATION = 3.0

const CAMERA_V_OFFSET_FACTOR = 0.3

const LAND_SHAKE_AMPLITUDE = 0.05
const LAND_SHAKE_DURATION = 0.3
const LAND_SHAKE_ITERATIONS = 5

const JUMP_OVERSCALE_WAVE1 = 0.8
const JUMP_OVERSCALE_WAVE2 = 0.2
const JUMP_OVERSCALE_ITERATIONS = 3

const BOOST_SHAKE_AMPLITUDE = 0.2
const BOOST_SHAKE_DURATION = 0.3
const BOOST_SHAKE_ITERATIONS = 5

const SCORE_HEIGHT = 0.08
const PROGRESS_HEIGHT = 0.04
const MESSAGE_HEIGHT = 0.2
const MESSAGE_Y_CENTER = 0.25
const FPS_HEIGHT = 0.025

export var jump_button_relative_size = 1/3.5
export var jump_margin_relative_size = 0.05

# Called when the node enters the scene tree for the first time.
func _ready():
	global = get_node("/root/Global")
	scores = scores_class.new()
	$World/Ball.update_player_best_score(scores.get_best_score_points())
	$World/Ball.set_steer_factor(global.rotation_speed)
	$World/Tunnel.setup("res://scenes/Rail.tscn", global.skill, scores.get_best_score_points())
	$Input.set_jump_stickiness_factor(1.0)
	var sensibility_sign = 1.0
	if global.invert_x:
		sensibility_sign = -1.0
	$Input.set_mouse_sensibility_factor(sensibility_sign*global.input_sensibility)
	$Input.set_keyboard_sensibility_factor(sensibility_sign*global.input_sensibility)
	$Effects/CRT.visible = false
	$UI/CenterContainer/ScoresContainer.visible = false		
	if $EscBar/Buttons/EscButton.connect("Esc", self, "on_escape"):
		pass
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()
	if $World/Ball.connect("start", self, "_on_Ball_start"):
		pass
	if $World/Ball.connect("jump", self, "_on_Ball_jump"):
		pass
	if $World/Ball.connect("land", self, "_on_Ball_land"):
		pass
	if $World/Ball.connect("fall", self, "_on_Ball_fall"):
		pass
	if $World/Ball.connect("catch_up", self, "_on_Ball_catch_up"):
		pass
	if $World/Ball.connect("change_column", self, "_on_Ball_change_column"):
		pass
	if $World/Ball.connect("boost", self, "_on_Ball_boost"):
		pass
	if $World/Ball.connect("overdrive", self, "_on_Ball_overdrive"):
		pass
	if $World/Ball.connect("thousand_score", self, "_on_Ball_thousand_score"):
		pass
	if $World/Ball.connect("percent_time", self, "_on_Ball_percent_time"):
		pass
	if $World/Ball.connect("high_score", self, "_on_Ball_high_score"):
		pass
	if $World/Ball.connect("finish", self, "_on_Ball_finish"):
		pass

func setup_layout():
	update_jump(0.0)
	update_score()
	update_progress()
	update_message(0.0)
	update_fps()

func update_score():
	current_score = $World/Ball.score()
	$UI/Score.text=str(current_score)
	var size = get_viewport_rect().size
	var wh = int((size.x + size.y)/2)
	var scale = wh*SCORE_HEIGHT/$UI/Score.rect_size.y
	$UI/Score.rect_size=Vector2(1,1)
	$UI/Score.rect_scale.x=scale
	$UI/Score.rect_scale.y=scale
	$UI/Score.rect_position.x = size.x-$UI/Score.rect_size.x*scale
	$UI/Score.rect_position.y = 0

func update_progress():
	if final_score:
		$UI/Progress.visible = false
	time_progress_done = $World/Ball.time_progress_done($World/Tunnel)
	time_progress_total = $World/Ball.time_progress_total()
	$UI/Progress.text=("%0.2f"%time_progress_done)+"/"+("%d"%int(round(time_progress_total)))
	var size = get_viewport_rect().size
	var wh = int((size.x + size.y)/2)
	$UI/Progress.rect_size=Vector2(1,1)
	var scale = wh*PROGRESS_HEIGHT/$UI/Progress.rect_size.y
	$UI/Progress.rect_scale.x=scale
	$UI/Progress.rect_scale.y=scale
	var total_x=$UI/Progress.rect_size.x+$UI/Progress.rect_size.y
	$UI/Progress.rect_position.x = size.x +($UI/Progress.rect_size.y/2 - total_x)*scale 
	$UI/Progress.rect_position.y = $UI/Score.rect_size.y*$UI/Score.rect_scale.y
	$UI/ProgressDone.rect_scale=$UI/Progress.rect_scale
	$UI/ProgressLeft.rect_scale=$UI/Progress.rect_scale
	$UI/ProgressDone.rect_position.x=size.x-total_x*scale
	$UI/ProgressDone.rect_position.y=$UI/Progress.rect_position.y
	$UI/ProgressDone.rect_size.y=$UI/Progress.rect_size.y
	$UI/ProgressLeft.rect_position.y=$UI/Progress.rect_position.y
	$UI/ProgressLeft.rect_size.y=$UI/Progress.rect_size.y
	if time_progress_total>0:
		$UI/ProgressDone.rect_size.x=total_x*time_progress_done/time_progress_total
		$UI/ProgressLeft.rect_position.x=$UI/ProgressDone.rect_position.x+$UI/ProgressDone.rect_size.x*scale
		$UI/ProgressLeft.rect_size.x=total_x-$UI/ProgressDone.rect_size.x
		
func update_fps():
	$UI/FPS.visible = global.show_fps
	if not global.show_fps:
		return
	$UI/FPS.text="fps: "+str(Performance.get_monitor(Performance.TIME_FPS))
	var size = get_viewport_rect().size
	var wh = int((size.x + size.y)/2)
	var scale = wh*FPS_HEIGHT/$UI/FPS.rect_size.y
	$UI/FPS.rect_scale.x=scale
	$UI/FPS.rect_scale.y=scale
	$UI/FPS.rect_position.x = size.x-$UI/FPS.rect_size.x*scale
	$UI/FPS.rect_position.y = size.y-$UI/FPS.rect_size.y*scale

func on_escape():
	if final_score:
		if get_tree().change_scene("res://scenes/MainMenu.tscn"):
			pass
	else:
		$Input.set_escape_mode(true)

func _process(delta):
	escape_mode = $Input.get_escape_mode()
	$UI/Escape.visible = escape_mode and not final_score
	$EscBar/Buttons.visible = (global.mobile_ui() or final_score) and not $UI/Escape.visible
	player_position=$World/Ball.position()
	$World/Tunnel.update_player_position(player_position)
	$World/Tunnel.set_level_row($World/Ball.level_row())
	$World/Tunnel.set_level_col($World/Ball.level_col($World/Tunnel.width()))
	$World/Tunnel.set_boost_state($World/Ball.is_boosting())
	$World/Ball.update_player_position(player_position)
	update_jump(delta)
	update_score()
	update_progress()
	update_fps()
	update_boost(delta)
	update_fall(delta)
	update_message(delta)
	update_camera(delta)
	update_finished(delta)

func update_camera(delta):
	delta_since_last_land += delta
	var last_land_v_offset=LAND_SHAKE_AMPLITUDE*math_fn.fade_out(delta_since_last_land/LAND_SHAKE_DURATION)*math_fn.triangle_wave(delta_since_last_land*LAND_SHAKE_ITERATIONS)
	$Cameras/MainCamera.v_offset = last_land_v_offset
	var trans
	var rot
	var ball_y = $World/Ball.position().y
	if ball_y>0:
		delta_since_last_boost += delta
		var last_boost_h_offset=BOOST_SHAKE_AMPLITUDE*math_fn.fade_out(delta_since_last_boost/BOOST_SHAKE_DURATION)*math_fn.triangle_wave(delta_since_last_boost*BOOST_SHAKE_ITERATIONS)
		$Cameras/MainCamera.h_offset = last_boost_h_offset
		var blend = min(1.0, ball_y)
		var trans1=$Cameras/PlayCamera.translation
		var trans2=$Cameras/JumpCamera.translation
		trans = trans1.linear_interpolate(trans2, blend)
		var rot1=$Cameras/PlayCamera.rotation
		var rot2=$Cameras/JumpCamera.rotation
		rot = rot1.linear_interpolate(rot2,blend)
	else:
		var blend = $World/Ball.backing_camera_blend()
		var trans1=$Cameras/PlayCamera.translation
		var trans2=$Cameras/FallCamera.translation
		trans = trans1.linear_interpolate(trans2, blend)
		var rot1=$Cameras/PlayCamera.rotation
		var rot2=$Cameras/FallCamera.rotation
		rot = rot1.linear_interpolate(rot2,blend)
	$Cameras/MainCamera.set_translation(trans)
	$Cameras/MainCamera.set_rotation(rot)

func update_boost(delta):
	if not is_boosting:
		$PreEffects/BoostFlash.visible=false
		return
	if boost_acc>1.0:
		is_boosting = false
		if global.use_shaders:
			$PreEffects/BoostFlash.visible=true
		return
	boost_acc += delta/BOOST_DURATION
	if global.use_shaders:
		$PreEffects/BoostFlash.visible=true
		$PreEffects/BoostFlash.material.set_shader_param("blend", boost_acc)

func update_jump(delta):
	jump_acc += delta/JUMP_OVERSCALE_DURATION
	var overscale1 = 1.0+(
		math_fn.fade_out(jump_acc)*
		math_fn.sawtooth_wave(jump_acc*JUMP_OVERSCALE_ITERATIONS)*
		JUMP_OVERSCALE_WAVE1)
	var overscale2 = 1.0+(
		math_fn.fade_out(jump_acc)*
		math_fn.sawtooth_wave(jump_acc*JUMP_OVERSCALE_ITERATIONS)*
		JUMP_OVERSCALE_WAVE2)
	
	var viewport_rect = get_viewport_rect().size
	var base_size = (viewport_rect.x + viewport_rect.y)/2
	var button_size1 = Vector2(
		base_size * jump_button_relative_size,
		base_size * jump_button_relative_size)
	var button_size2 = Vector2(
		base_size * jump_button_relative_size,
		base_size * jump_button_relative_size)
	var margin_size = Vector2(
		viewport_rect.x * jump_margin_relative_size + max(button_size1.x,button_size2.x)/2.0,
		viewport_rect.y * jump_margin_relative_size + max(button_size1.y,button_size2.y)/2.0)
	button_size1 *= overscale1
	button_size2 *= overscale2
	var size1 = $MobileUI/JumpButton/JumpButton1.rect_size
	var scale1 = Vector2(button_size1.x/size1.x,button_size1.y/size1.y)
	var size2 = $MobileUI/JumpButton/JumpButton2.rect_size
	var scale2 = Vector2(button_size2.x/size2.x,button_size2.y/size2.y)
	var position1 = Vector2(margin_size.x-button_size1.x/2, viewport_rect.y+button_size1.y/2-(button_size1.y+margin_size.y))
	var position2 = Vector2(margin_size.x-button_size2.x/2, viewport_rect.y+button_size2.y/2-(button_size2.y+margin_size.y))
	if global.left_handed:
		position1.x = viewport_rect.x - button_size1.x - position1.x
		position2.x = viewport_rect.x - button_size2.x - position2.x
	$MobileUI/JumpButton/JumpButton1.rect_position = position1
	$MobileUI/JumpButton/JumpButton2.rect_position = position2
	$MobileUI/JumpButton/JumpButton1.rect_scale = scale1
	$MobileUI/JumpButton/JumpButton2.rect_scale = scale2
	$Input.set_jump_button(position2, size1, scale1)
	$Input.set_viewport_rect(viewport_rect)
	$MobileUI/JumpButton.visible = global.mobile_ui() and not escape_mode

func update_fall(delta):
	if not is_falling:
		$PreEffects/FallFlash.visible=false
		return
	if fall_acc>1.0:
		is_falling = false
		if global.use_shaders:
			$PreEffects/FallFlash.visible=true
		return
	fall_acc += delta/FALL_DURATION
	if global.use_shaders:
		$PreEffects/FallFlash.visible=true
		$PreEffects/FallFlash.material.set_shader_param("blend", fall_acc)

func update_finished(delta):
	if not finished:
		return
	if finished_acc>1.0:
		if not final_score:
			final_score = true
			$UI/Score.visible = false
			$UI/Progress.visible = false
			message_txt_acc = 1.1
			$Input.disable()
			$UI/CenterContainer/ScoresContainer.visible = true
			$UI/CenterContainer/ScoresContainer/ScoresList.update_scores()
	finished_acc += delta/FINISH_DURATION

func set_message(msg):
	message_txt_acc=0
	message_fx_acc=0
	$UI/Message.text = msg
	
func reset_message():
	message_txt_acc=1.1
	message_fx_acc=1.1
	$UI/Message.text = ""
	
func update_message(delta):
	update_fx_message(delta)
	update_txt_message(delta)
	
func update_fx_message(delta):
	if message_fx_acc>1.0:
		$PreEffects/MessageFlash.visible=false
		return
	message_fx_acc += delta/MESSAGE_FX_DURATION
	if global.use_shaders:
		$PreEffects/MessageFlash.visible=true
		$PreEffects/MessageFlash.material.set_shader_param("blend", message_fx_acc)

func update_txt_message(delta):
	if message_txt_acc>1.0:
		$UI/Message.visible=false
		return
	if not finished:
		# last message stays forever
		message_txt_acc += delta/MESSAGE_TXT_DURATION
	$UI/Message.visible = true
	var size = get_viewport_rect().size
	var wh = int((size.x + size.y)/2)
	var scale = wh*MESSAGE_HEIGHT/$UI/Message.rect_size.y
	$UI/Message.rect_scale.x = scale
	$UI/Message.rect_scale.y = scale
	$UI/Message.rect_position.x = size.x/2 -$UI/Message.rect_size.x*scale/2
	$UI/Message.rect_position.y = size.y*MESSAGE_Y_CENTER - $UI/Message.rect_size.y*scale/2

func _physics_process(delta):
	if finished or escape_mode:
		return
	$World/Ball.tick(delta, $World/Tunnel, $Input)

func _on_Menu_back():
	$Input.set_escape_mode(false)
	if not score_saved:
		scores.add_score({"points": current_score, "skill": global.skill})
		scores.save_scores()
		score_saved = true
	if get_tree().change_scene("res://scenes/MainMenu.tscn"):
		pass

func _on_Play_resume():
	$Input.set_escape_mode(false)

func _on_Restart_restart():
	if not score_saved:
		scores.add_score({"points": current_score, "skill": global.skill})
		scores.save_scores()
		score_saved = true
	if get_tree().change_scene("res://scenes/Playing.tscn"):
		pass

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST || what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		$Input.set_escape_mode(true)

func _on_Ball_jump():
	$SFX/Jump.play()
	delta_since_last_jump = 0.0
	jump_acc = 0.0

func _on_Ball_land():
	$SFX/Land.play()
	delta_since_last_land = 0.0

func _on_Ball_fall():
	$SFX/Fall.play()
	if global.use_shaders:
		$Effects/CRT.visible = true
	$World/Tunnel.set_fall_state(true)
	fall_acc = 0.0;
	is_falling = true;
	$Input.flush()
	reset_message()
	
func _on_Ball_start():
	$SFX/Start.play()
	$Input.flush()
	set_message("Go!")

func _on_Ball_catch_up():
	$SFX/CatchUp.play()
	$Effects/CRT.visible = false
	$World/Tunnel.set_fall_state(false)
	set_message("Back on track")

func _on_Ball_change_column():
	$SFX/ChangeColumn.play()

func _on_Ball_boost():
	$SFX/Boost.play()
	delta_since_last_boost = 0.0
	boost_acc = 0.0;
	is_boosting = true;
	set_message("Boost!")

func _on_Ball_overdrive():
	$SFX/Overdrive.play()
	delta_since_last_boost = 0.0
	boost_acc = 0.0;
	is_boosting = true;
	set_message("Overdrive!!!")

func _on_Ball_percent_time(percent_time):
	$SFX/PercentTime.play()
	var msg="Progress: %d%%" % percent_time
	set_message(msg)

func _on_Ball_thousand_score(score):
	$SFX/ThousandScore.play()
	current_score=score
	var msg="Score: %d" % score
	set_message(msg)

func _on_Ball_finish(score):
	finished=true
	current_score=score
	scores.add_score({"points": score, "skill": global.skill})
	scores.save_scores()
	score_saved = true
	if new_record:
		$SFX/FinishNewRecord.play()
		set_message("New record!")
	else:
		$SFX/FinishGameOver.play()
		set_message("Game over")

func _on_Ball_high_score(score):
	$SFX/HighScore.play()
	current_score=score
	new_record=true
	set_message("Record: %s" % score)

