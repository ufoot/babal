extends ColorRect

func _ready():
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()

func setup_layout():
	self.rect_position.x = 0
	self.rect_position.y = 0
	self.rect_size.x = get_viewport_rect().size.x
	self.rect_size.y = get_viewport_rect().size.y
