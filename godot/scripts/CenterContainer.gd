extends CenterContainer

func _ready():
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()

func setup_layout():
	var viewport_size = get_viewport_rect().size
	self.rect_position.x = 0
	self.rect_position.y = 0
	self.rect_size.x = viewport_size.x
	self.rect_size.y = viewport_size.y
	
