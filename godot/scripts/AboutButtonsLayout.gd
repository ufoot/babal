extends Node

func _ready():
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()

func _process(_delta):
	setup_layout()

func setup_layout():
	var viewport_size = get_parent().get_viewport_rect().size
	var center = Vector2(viewport_size.x / 2, viewport_size.y /2)
	var wh = (viewport_size.x+viewport_size.y)/14
	for button in get_children():
		button.rect_scale.x = wh/button.rect_size.x
		button.rect_scale.y = wh/button.rect_size.y
	$Babal.rect_position.x = center.x - 2.75*wh
	$Babal.rect_position.y = viewport_size.y - wh
	$GodotEngine.rect_position.x = center.x - 1.25*wh
	$GodotEngine.rect_position.y = viewport_size.y - wh
	$GodotRust.rect_position.x = center.x + 0.25*wh
	$GodotRust.rect_position.y = viewport_size.y - wh
	$SerpentSoundStudio.rect_position.x = center.x + 1.75*wh
	$SerpentSoundStudio.rect_position.y = viewport_size.y - wh
