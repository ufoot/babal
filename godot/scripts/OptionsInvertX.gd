extends CheckButton

var global

func _ready():
	global = get_node("/root/Global")
	pressed = global.invert_x

func _toggled(button_pressed):
	global.invert_x = bool(button_pressed)
	global.save_config()
