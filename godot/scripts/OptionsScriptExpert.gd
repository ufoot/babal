extends CheckBox

var global

func _ready():
	global = get_node("/root/Global")
	if global.skill == "expert":
		self.pressed = true
		
func _pressed():
	global.skill = "expert"
	global.save_config()
