extends GridMap

var global

const MAX_ITEM = 4
const MATERIAL_FLAVORS = 10

func _ready():
	global = get_node("/root/Global")
	setup_mesh_library()
	
func setup_mesh_library():
	var n = mesh_library.get_last_unused_item_id()
	if n>MAX_ITEM:
		return
	var mesh_library = self.mesh_library
	for i in range(MATERIAL_FLAVORS-1):
		var blend = float(i+1)/float(MATERIAL_FLAVORS-1)
		for item in range(n):
			var mesh=mesh_library.get_item_mesh(item)
			mesh_library.create_item((i+1)*n+item)
			var new_mesh=mesh.duplicate()
			var new_material=new_mesh.material.duplicate()
			var color = new_material.albedo_color
			new_material.emission_enabled = true
			new_material.emission = color
			new_material.emission_energy = blend*blend
			new_mesh.material=new_material
			mesh_library.set_item_mesh(item+n*(i+1), new_mesh)

