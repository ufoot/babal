extends Control

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	if $EscBar/Buttons/EscButton.connect("Esc", self, "on_escape"):
		pass

func on_escape():
	if get_tree().change_scene("res://scenes/MainMenu.tscn"):
		pass
