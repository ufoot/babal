extends TabContainer

func _ready():
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()

func setup_layout():
	self.rect_position.x = 0
	self.rect_position.y = 0
	self.rect_min_size.x = get_viewport_rect().size.x*0.8
	self.rect_min_size.y = get_viewport_rect().size.y*0.8
	

