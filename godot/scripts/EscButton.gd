extends TextureButton

signal Esc

func _pressed():
	emit_signal("Esc")
