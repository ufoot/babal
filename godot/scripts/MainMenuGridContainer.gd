extends HBoxContainer

func _ready():
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()

func setup_layout():
	var viewport_size = get_viewport_rect().size
	var wh = (viewport_size.x+viewport_size.y)/10
	for button in get_children():
		button.rect_scale.x = wh/button.rect_size.x
		button.rect_scale.y = wh/button.rect_size.y
		button.rect_min_size.x = wh
		button.rect_min_size.y = wh
		button.rect_size.x = wh
		button.rect_size.y = wh

