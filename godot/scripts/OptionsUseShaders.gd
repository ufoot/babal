extends CheckButton

var global

func _ready():
	global = get_node("/root/Global")
	pressed = global.use_shaders

func _toggled(button_pressed):
	global.use_shaders = bool(button_pressed)
	global.save_config()
