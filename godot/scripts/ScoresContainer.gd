extends VBoxContainer

func _ready():
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()
	$ScoresList.update_scores()

func setup_layout():
	self.rect_min_size.x = 0.9*get_viewport_rect().size.x
