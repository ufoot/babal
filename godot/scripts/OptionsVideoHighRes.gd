extends CheckBox

const WIDTH = 1920
const HEIGHT = 1080

var global

func _ready():
	global = get_node("/root/Global")

func _process(_delta):
	var ws = OS.get_window_size() 
	pressed = (ws.x == WIDTH && ws.y == HEIGHT) and not OS.window_fullscreen
	disabled = global.mobile_ui()

func _pressed():
	OS.set_window_size(Vector2(WIDTH, HEIGHT))
	global.fullscreen = false
	global.save_config()
	get_tree().get_root().set_size(Vector2(WIDTH-1, HEIGHT-1))	
	get_tree().get_root().set_size(Vector2(WIDTH, HEIGHT))	
