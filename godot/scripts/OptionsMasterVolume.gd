extends VSlider

var global

func _ready():
	global = get_node("/root/Global")
	value = global.master_volume
	_on_MasterVolume_value_changed(value)
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()

func setup_layout():
	self.rect_position.x = 0
	self.rect_position.y = 0
	self.rect_min_size.x = get_viewport_rect().size.x*0.2
	self.rect_min_size.y = get_viewport_rect().size.y*0.5
	
func _on_MasterVolume_value_changed(value):
	global.master_volume = float(value)
	global.save_config()
