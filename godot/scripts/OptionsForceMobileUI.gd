extends CheckButton

var global

func _ready():
	global = get_node("/root/Global")
	pressed = global.force_mobile_ui

func _toggled(button_pressed):
	global.force_mobile_ui = bool(button_pressed)
	global.save_config()
