extends CheckBox

var global

func _ready():
	global = get_node("/root/Global")
	if global.skill == "easy":
		self.pressed = true
		
func _pressed():
	global.skill = "easy"
	global.save_config()
