extends VBoxContainer

var scores_class = preload("res://scripts/Scores.gd")

func update_scores():
	var scores = scores_class.new()
	for i in range(0, scores.NB_SCORES):
		var child = get_child(i+1)
		if not child:
			return
		var score = scores.get_score(i)
		if (not score.has("points")) or score["points"] <= 0:
			child.text = "..."
		else:
			child.text=scores.pretty_print(i)

