extends HSlider

var global

func _ready():
	global = get_node("/root/Global")
	value = global.rotation_speed
	if get_tree().get_root().connect("size_changed", self, "setup_layout"):
		pass
	setup_layout()

func setup_layout():
	self.rect_position.x = 0
	self.rect_position.y = 0
	self.rect_min_size.x = get_viewport_rect().size.x*0.35
	self.rect_min_size.y = 0
	
func _on_HSliderRotationSpeed_value_changed(value):
	global.rotation_speed = float(value)
