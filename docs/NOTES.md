Usefull commands
----------------

Sign the APK:

```sh
java -jar ~/Home/bin/uber-apk-signer-1.2.1.jar -a export/babal-android-v0.2.1.apk --ks ~/.android/ufoot.keystore --ksAlias upload
```

Resources
---------

* [Pointfree font](http://www.publicdomainfiles.com/show_file.php?id=13501609212966)

* [archives of pdsounds.org](http://pdsounds.tuxfamily.org/)

* [sfxr](https://www.drpetter.se/project_sfxr.html)

* [Space Ambience](https://freepd.com/music/Space%20Ambience.mp3) by [Alexander Nakarada](https://www.serpentsoundstudios.com/)
