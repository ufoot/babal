Babal
=====

![Babal screenshot](https://gitlab.com/ufoot/babal/raw/master/hp48/screenshot-hp48.jpg)

Babal is a bouncing ball game.

This is the source code for the
[HP48 version](https://ufoot.org/software/babal/hp48) of
[Babal](https://ufoot.org/software/babal).

For more context on what an HP48, it is a
[pocket calculator from the 90s](https://en.wikipedia.org/wiki/HP_48_series).

Status
------

This is really abandonware, I'm really not maintaining this any more.
Source code dates back from 1995. Yes, 1995.
Just sharing because I have no reasons not to share it.

If you want a practical, real world example of "why assembly code is not maintainable",
just [read this](https://gitlab.com/ufoot/babal/blob/master/hp48/src/BABALDEV.DIR/MAIN/AFFICH) ;)

Documentation
-------------

* [HP48 Manual](https://ufoot.org/software/babal/hp48/manual)

Authors
-------

* Christian Mauduit <ufoot@ufoot.org> : main developper, project
  maintainer.

License
-------

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to [unlicense.org](https://unlicense.org)
