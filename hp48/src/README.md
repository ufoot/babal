The content in this directory come from [BABALDEV.DIR](https://gitlab.com/ufoot/babal/blob/master/hp48/BABALDEV.DIR)
and have been extracted with [Splitdir 3.0](https://www.hpcalc.org/hp48/pc/misc/splitdir30.tgz).
It's a bit blurry but the main, interesting, source code, is located in [MAIN](https://gitlab.com/ufoot/babal/blob/master/hp48/src/BABALDEV.DIR/MAIN).
No real clue how to explain or debug this. I just know it works, it's a raw copy of my working dir back in the time.

[ufoot](mailto:ufoot@ufoot.org).
