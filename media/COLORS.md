Colors used within the game
---------------------------

* `#000000` black
* `#ffffff` white
* `#292929` light grey
* `#d6d6d6` dark grey
* `#29d6d6` blue, on sprites
* `#d629d6` purple, on sprites
* `#00ffff` blue, on 3D objects
* `#ff00ff` purple, on 3D objects
