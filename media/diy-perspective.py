#!/usr/bin/env python3

import math
import sys

def point_coords(x, y):
    dir=(math.cos(((x+67.5)/180.0)*math.pi),math.sin(((x+67.5)/180.0)*math.pi))
    dist=1/(1+y)
    return (round(128+120*dist*dir[0],2), round(128+120*dist*dir[1],2))

def slab_coords(n, y1, y2):
    x1 = float(n)*45.0 + 2.5
    x2 = (float(n)+1)*45.0 - 2.5
    y1 = float(y1)
    y2 = float(y2)
    return (point_coords(x1, y1),
            point_coords(x1, y2),
            point_coords(x2, y2),
            point_coords(x2, y1))

if __name__=="__main__":
    coords = slab_coords(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))
    print(coords)
